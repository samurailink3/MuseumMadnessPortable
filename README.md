# Museum Madness Portable
## The classic MECC game in a nice portable DOSBox-driven package.

### Instructions

**This repository is broken into branches based off of platform. Please select the branch for your platform of choice.**

Currently supported platforms:

* Windows XP and above (x86 and x64)
* Linux x64

Pull a [ZIP from GitHub](https://github.com/samurailink3/MuseumMadnessPortable/archive/windows.zip), unzip, and double-click on `MUSEUM.BAT`. The game will launch in DOSBox and you'll be ready to play!

Total unzipped size is about `13.4 MB`, give or take.

### Information

[Read up on Museum Madness](http://en.wikipedia.org/wiki/Museum_Madness_\(video_game\))

### Credits

A huge thanks to [Abandonia.com](http://www.abandonia.com/en/games/479/Museum+Madness.html) for hosting the game!

Kudos to the great folks over at [DOSbox](http://www.dosbox.com/) for making such an awesome emulator.
